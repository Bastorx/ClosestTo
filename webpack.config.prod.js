const { merge } = require("lodash");
const config = require("./webpack.config");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = merge(config, {
  mode: "production",
  optimization: {
    minimize: true
  },
  plugins: config.plugins.concat([new CleanWebpackPlugin()])
});
