import { map, slice } from "lodash";
import closeTo from "./src/utils/closestTo";

const args = slice(process.argv, 2);

const numberArray: number[] = map<string, number>(args, (n: string) =>
  parseFloat(n)
);

console.info("The initial array is :", numberArray);

console.info("Le nombre le plus proche de 0 est :", closeTo(numberArray));
