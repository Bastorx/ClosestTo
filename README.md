# ClosestTo

The initial project was to make a function that return the number "closest To Zero".
I improved the project to make it become "Closest To Any Given Number".

This app work with a react front-end but it can be used only with the terminal.

```
npm install
```

To run the project from terminal (Closest To 0)

```
npm start -- 8 12.1 5 74 -45 45
```

To run the web version (default port: 3000)

```
npm run build
npm run web
```

To run unit test

```
npm test
```

To run developer server webpack

```
npm run dev
```

Be sure to have node / npm installed

## What could be improved?

*  Docker integration
*  Optimisation webpack (or maybe think about if another technology like Gatsby could be interesting)
*  Test End2End (with Cypress?)