import React, { Component } from "react";
import { Container, Row } from "react-grid-system";
import { isNumber } from "lodash";
import styled from "styled-components";

import ClosestToForm from "../components/CloseTo/ClosestToForm";
import Submit from "../components/CloseTo/Submit";
import Result from "../components/CloseTo/Result";
import closeTo from "../utils/closestTo";

interface IProps {
  configurableNumber?: boolean;
}

interface IState {
  result: number | null;
  objective: number | null;
}

const FlexRow = styled(Row)`
  display: flex;
  justify-content: center !important;
  margin-top: 20px;
`;

class ClosestToZero extends Component<IProps, IState> {
  state: IState = {
    result: null,
    objective: null
  };
  onSubmit = (numberArray: number[], objective = 0) => {
    this.setState({
      result: closeTo(numberArray, objective),
      objective
    });
  };
  render() {
    const { configurableNumber } = this.props;
    const { result, objective } = this.state;
    return (
      <Container>
        <FlexRow>
          <ClosestToForm
            configurableNumber={configurableNumber}
            onSubmit={this.onSubmit}
          />
        </FlexRow>
        {isNumber(result) && (
          <FlexRow>
            <Result result={result} objective={objective || 0} />
          </FlexRow>
        )}
      </Container>
    );
  }
}

export default ClosestToZero;
