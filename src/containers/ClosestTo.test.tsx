import React from "react";
import renderer from "react-test-renderer";
import CloseTo from "./ClosestTo";

describe("Test CloseTo Containers with snapshot", () => {
  it("renders correctly CloseTo without configurable objective", () => {
    const tree = renderer.create(<CloseTo />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders correctly CloseTo with configurable objective", () => {
    const tree = renderer.create(<CloseTo configurableNumber />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
