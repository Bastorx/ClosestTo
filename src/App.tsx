import * as React from "react";
import ReactDOM from "react-dom";

import "./style.css";

import Router from "./Router";

ReactDOM.render(<Router />, document.getElementById("root"));
