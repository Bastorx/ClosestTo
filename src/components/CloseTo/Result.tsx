import React from "react";
// import styled from "styled-components";

interface IProps {
  result: number;
  objective: number;
}
const Result = ({ result, objective }: IProps) => (
  <div>
    <p>
      Le nombre le plus proche de {objective} est {result}
    </p>
  </div>
);

export default Result;
