import React, { useState } from "react";
import styled from "styled-components";
import { isFinite } from "lodash";

const Input = styled.input`
  display: block;
  padding: 10px;
  background-color: white;
  border-radius: 5px;
  border: 2px #1a6fc9 solid;
  width: 276px;
  margin-top: 20px;
`;

interface IProps {
  objective: number;
  setObjective: (objective: number) => void;
}

const ObjectiveInput = ({ objective, setObjective }: IProps) => {
  const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
    let val = e.currentTarget.value;
    val = val.replace(/,/g, ".");
    const regex = /^[-+]?\d*\.?\d*$/;
    if (regex.exec(val)) {
      setObjective(parseFloat(val));
    }
  };
  return (
    <Input
      type="text"
      placeholder="Objective number"
      onChange={handleChange}
      value={isFinite(objective) ? objective.toString() : ""}
    />
  );
};

export default ObjectiveInput;
