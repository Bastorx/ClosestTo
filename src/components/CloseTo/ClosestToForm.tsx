import React, { useState } from "react";
import { map } from "lodash";
import ObjectiveInput from "./ObjectiveInput";
import Submit from "./Submit";

import { WithContext as ReactTags, Tag } from "react-tag-input";

interface IProps {
  configurableNumber?: boolean;
  onSubmit: (numberArray: number[], objective?: number) => void;
}

const KeyCodes = {
  tab: 9,
  space: 32,
  enter: 13
};

const delimiters = [KeyCodes.space, KeyCodes.enter, KeyCodes.tab];

const ClosestToForm = ({ onSubmit, configurableNumber }: IProps) => {
  const [tags, setTags] = useState<Tag[]>([]);
  const [value, setValue] = useState<string>("");
  const [objective, setObjective] = useState<number>(0);
  const handleAddition = (t: Tag) => {
    setTags([...tags, { id: t.id, text: value }]);
    setValue("");
  };
  const handleDelete = (i: number) => {
    setTags(tags.filter((tag, index) => index !== i));
  };
  const handleInputChange = (val: string) => {
    val = val.replace(/,/g, ".");
    const regex = /^[-+]?\d*\.?\d*$/;
    if (regex.exec(val)) {
      setValue(val);
    }
  };
  const handleSubmit = () => {
    onSubmit(
      map<Tag, number>(tags, (tag) => parseFloat(tag.text)),
      configurableNumber ? objective : undefined
    );
  };

  return (
    <div>
      <ReactTags
        tags={tags}
        handleDelete={handleDelete}
        handleAddition={handleAddition}
        allowDragDrop={false}
        delimiters={delimiters}
        allowUnique={false}
        handleInputChange={handleInputChange}
        inputValue={value}
      />
      {configurableNumber && (
        <ObjectiveInput objective={objective} setObjective={setObjective} />
      )}
      <Submit onClick={handleSubmit}>Compute</Submit>
    </div>
  );
};

export default ClosestToForm;
