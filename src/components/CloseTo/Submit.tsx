import React from "react";
import styled from "styled-components";

const Submit = styled.button`
  padding: 10px;
  background-color: white;
  border-radius: 5px;
  border: 2px #1a6fc9 solid;
  width: 300px;
  margin-top: 20px;
`;

export default Submit;
