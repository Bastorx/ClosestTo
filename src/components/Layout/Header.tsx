import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Header = styled.header`
  background-color: #1a6fc9;
`;

const Nav = styled.nav`
  display: flex;
  color: white;
`;

const LinkStyled = styled(Link)`
  flex: 1;
  color: white;
  text-align: center;
  padding: 20px;
  text-decoration: none;
  font-size: 1.5rem;
  font-weight: bold;
  transition: all 0.5s;
  &:hover {
    box-shadow: inset 0px 0px 0px 5px #14fcd9;
  }
`;

const HeaderComponent = () => (
  <Header>
    <Nav>
      <LinkStyled to="closest-to-zero">Closest to zero</LinkStyled>
      <LinkStyled to="closest-to-any">Closest to any number</LinkStyled>
    </Nav>
  </Header>
);

export default HeaderComponent;
