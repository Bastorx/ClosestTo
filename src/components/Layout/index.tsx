import React from "react";
import Header from "./Header";
import Footer from "./Footer";

interface IProps {
  children: React.ReactNode;
}
const Layout = ({ children }: IProps) => (
  <>
    <Header />
    {children}
    <Footer />
  </>
);

export default Layout;
