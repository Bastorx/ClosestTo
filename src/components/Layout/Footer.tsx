import React from "react";
import styled from "styled-components";

const Footer = styled.footer`
  background-color: #1a6fc9;
  padding: 50px 0;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

const Content = styled.div`
  text-align: center;
  color: white;
`;

const Margin = styled.div`
  height: 200px;
`;

const FooterComponent = () => (
  <>
    <Margin />
    <Footer>
      <Content>
        <p>Développé par</p>
        <p>Bastien CHEVALLIER</p>
      </Content>
    </Footer>
  </>
);

export default FooterComponent;
