import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import ClosestTo from "./containers/ClosestTo";
import NotFoundPage from "./components/NotFoundPage";
import Layout from "./components/Layout";

class RouterComponent extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Redirect exact from="/" to="/closest-to-zero" />
          <Route
            exact
            path="/closest-to-any"
            component={() => (
              <Layout>
                <ClosestTo configurableNumber />
              </Layout>
            )}
          />
          <Route
            exact
            path="/closest-to-zero"
            component={() => (
              <Layout>
                <ClosestTo />
              </Layout>
            )}
          />
          <Route component={() => <NotFoundPage />} />
        </Switch>
      </Router>
    );
  }
}

export default RouterComponent;
