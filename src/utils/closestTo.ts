import { isArray, isEmpty, isNumber } from "lodash";

const difference = (a: number, b: number) => Math.abs(a - b);

const closeTo = (numberArray: number[], close = 0) => {
  if (!isArray(numberArray) && isEmpty(numberArray)) {
    return 0;
  }
  let numberFound: number | undefined;

  for (const element of numberArray) {
    if (!isNumber(element)) {
      return;
    } else if (!isNumber(numberFound)) {
      numberFound = element;
    } else if (difference(close, numberFound) >= difference(close, element)) {
      if (element === -numberFound) {
        numberFound = numberFound > element ? numberFound : element;
      } else {
        numberFound = element;
      }
    }
    if (numberFound === close) {
      break;
    }
  }

  // If still undefined return 0
  return numberFound || 0;
};

export default closeTo;
