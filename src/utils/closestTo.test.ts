import closestTo from "./closestTo"

describe('Should test closestTo function', () => {
    test('[0, 1, 2, 3, 4] should be 0', () => {
    expect(closestTo([0, 1, 2, 3, 4])).toBe(0);
    });

    test('[-10, -11, -211, -9, -44] should be 0', () => {
        expect(closestTo([-10, -11, -211, -9, -44])).toBe(-9);
    });

    test('[-10, 8, -2, 30, 450] should be -2', () => {
        expect(closestTo([-10, 8, -2, 30, 450])).toBe(-2);
    });

    test('[-Infinity, Infinity] & [Infinity, -Infinity] should be Infinity', () => {
        expect(closestTo([Infinity, -Infinity])).toBe(Infinity);
        expect(closestTo([-Infinity, Infinity])).toBe(Infinity);
    });

    test('[0, 1, 2, 3, 4] should be 3 because 2nd parameters is 3', () => {
        expect(closestTo([0, 1, 2, 3, 4], 3)).toBe(3);
    });

    test('[-10, -11, -211, -9, -44] should be -44 because 2nd parameters is -42', () => {
        expect(closestTo([-10, -11, -211, -9, -44], -42)).toBe(-44);
    });

    test('[-10, 8, -2, 30, 450] should be 450 because 2nd parameters is 50000', () => {
        expect(closestTo([-10, 8, -2, 30, 450], 50000)).toBe(450);
    });
});